---
title: "Skin"
date: 04/25/20 00:55:28
---

Code | Text | Summary | SNOMED
--- | --- | --- | ---
`@ACE` | Sections show features of actinic elastosis.  There is no evidence of dysplasia or malignancy.  | Actinic (solar) elastosis.  | `50010`
`@ACK` | Sections show mild / moderate / severe epidermal dysplasia (actinic keratosis).  There is no evidence of invasive malignancy.  | Actinic keratosis.  | `74007`
`@LIPL` | Sections show ??? skin/squamous mucosa with bandlike subepithelial lymphoid infiltrate, hydropic swelling of the basal layer cells and focal single keratinocyte necrosis. There is no evidence of dysplasia or malignancy.  | Appearances in keeping with lichen planus.  | `43000???`
`@BLUN` | Sections show skin.  The epidermis is unremarkable.  The dermis shows a focal lesion composed of dendritic and bipolar pigmented macrophages in a fibroblastic, dense stroma.  The features are of a blue naevus.  There is no evidence of atypia or mitosis.  There is no evidence of dysplasia or malignancy.  Excision appears complete.   | BLUE NAEVUS.  | `87800`
`@BCCX` | Sections show a ??? basal cell carcinoma.  No atypical squamous component seen. Tumour is  mm from the deep margin and  mm from nearest peripheral margin (o' clock / specimen not oriented). Depth of invasion is ???. Perineural/vascular invasion ???.  | Basal cell carcinoma. TNM (8) pT .  | `80903`
`@BEEP` | Benign epithelial lesion.  There is no evidence of dysplasia or malignancy.  | Benign epithelial lesion.  | `09350`
`@JUN` | Sections show a benign junctional naevus.  There is no evidence of dysplasia or malignancy. Excision appears complete.  | Benign junctional naevus.  | `87400`
`@CON` | Sections show features of a benign melanocytic compund naevus.  There is no evidence of malignancy. Excision appears complete.  | Benign melanocytic compund naevus.  | `87600`
`@IDN` | The skin shows features of a benign melanocytic intradermal naevus.  There is no evidence of malignancy. Excision appears complete.  | Benign melanocytic intradermal naevus.  | `87500`
`@BSQL` | Sections show features of a benign squamoproliferative lesion.  There is no evidence of dysplasia or malignancy.  | Benign squamoproliferative lesion.  | `09350`
`@BOWS` | Sections show severe epidermal dysplasia (Bowen'S diesase).  There is no evidence of invasive malignancy.  | Bowen's disease.  | `80812`
`@BOWK` | Sections show features of Bowenoid keratosis.  There is no evidence of invasive malignancy.  | Bowenoid keratosis.  | `80902`
`@PILS` | Sections show features consistent with a pilonidal sinus.  There is no evidence of dysplasia or malignancy.  | Consistent with a pilonidal sinus.  | `46350`
`@DEF` | Sections show features of a benign dermatofibroma.  There is no evidence of atypia, dysplasia or malignancy. Excision appears complete.  | Dermatofibroma, in-completely excised.  | `88320`
`@SEBC` | Sections show an epidermoid (sebaceous) cyst.  | Epidermoid (sebaceous) cyst.  | `33410`
`@STAS` | Sections show epidermal spongiosis and parakeratosis accompanied by mild perivascular chronic inflammation, dermal oedema and fibrosis; red blood cell extravasation/haemosiderin deposition is also noted. There is no evidence of dysplasia or malignancy.  | Features consistent with stasis dermatitis. Correlate clinically.  | `48010`
`@SFFI` | Sections show features of a fibroepithelial polyp with a central adipose tissue core, in keeping with a soft fibroma.  There is no evidence of dysplasia or malignancy.  | Fibroepithelial polyp with a central adipose tissue core, in keeping with a soft fibroma.  | `76810`
`@FEP` | Sections show features of a fibroepithelial polyp.  There is no evidence of dysplasia or malignancy.  | Fibroepithelial polyp.  | `76810`
`@HAEM` | Section show features of a haemangioma.  There is no evidence of atypia or malignancy.  | Haemangioma.  | `91200`
`@SLEN` | Sections show skin with epidermal hyperkeratosis, mild elongation and budding of rete ridges with some increase in melanocytes.  There is no evidence of dysplasia, cytological atypia or malignancy.   | In keeping with solar lentigo.  | `57250`
`@IBCC` | Sections show part of an infiltrative basal cell carcinoma.  | Infiltrative basal cell carcinoma.  | `80903`
`@NBCC` | Sections show part of a nodular basal cell carcinoma.  | Nodular basal cell carcinoma.  | `80903`
`@PIDN` | Sections show part of an intradermal naevus.  There is no evidence of dysplasia or malignancy.  | Partially excised intradermal naevus.  | `87500`
`@PILC` | Sections show a benign pilar cyst.  | Pilar cyst.  | `33410`
`@PYGR` | Sections show features of a pyogenic granuloma.  There is no evidence of dysplasia or malignancy.  | Pyogenic granuloma.  | `44440 or 91200`
`@SEBK` | Sections show features of a seborrhoeic keratosis.  There is no evidence of dysplasia or malignancy.  | Seborrhoeic keratosis.  | `72750`
`@QWRT` | Sections show part of a squamoproliferative lesion with some features suggestive of a viral aetiology.  There is no evidence of dysplasia or malignancy.  | Squamoproliferative lesion with some features suggestive of a viral aetiology.  | `76600`
`@SQP` | Sections show a squamous cell papilloma.  There is no evidence of dysplasia or malignancy.  | Squamous cell papilloma.  | `80520`
`@SBCC` | Sections show part of a superficial basal cell carcinoma.  | Superficial basal cell carcinoma.  | `80903`
`@WART` | Sections show features of a viral wart.  There is no evidence of dysplasia or malignancy.  | Viral wart.  | `76600`