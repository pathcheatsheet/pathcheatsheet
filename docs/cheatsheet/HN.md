---
title: "Head & Neck"
date: 04/25/20 00:55:28
---
	
Code | Text | Summary | SNOMED
--- | --- | --- | ---
`@ALNP` | Sections show a benign allergic nasal polyp lined by respiratory-type epithelium.  The subepithelial tissue is oedematous and myxoid and shows infiltration by mixed inflammatory cells, predominantly eosinophils.  There is no evidence of dysplasia or malignancy.  | Benign allergic nasal polyp.  | `76800`
`@INNP` | Sections show a benign inflammatory nasal polyp lined by respiratory-type epithelium.  The subepithelial tissue is oedematous and myxoid and shows infiltration by mixed inflammatory cells.  There is no evidence of dysplasia or malignancy.  | Benign inflammatory nasal polyp.  | `76800`
`@FRKS` | Sections show hyperplastic, hyperkeratotic squamous mucosa with some subepithelial fibrosis and chronic inflammation. There is no evidence of fungal infection, dysplasia or malignancy.  | In keeping with frictional keratosis.  | `09350`
`@PLAD` | Sections show a well circumscribed pleomorphic adenoma composed of myoepithelial cells in a chondromyxoid stroma.  There is no evidence of atypia or malignancy.  The lesion appears completely excised.  | Pleomorphic adenoma.  | `89400`
`@TONS` | Hyperplastic lymphoid tissues covered by unremarkable squamous epithelium. There is no evidence of lyphoproliferative disease, dysplasia or malignancy.  | Reactive lymphoid hyperplasia.  | `72000`
`@ADEN` | Sections show polypoidal fragments of hyperplastic lymphoid tissue lined by respiratory and focally by squamous epithelium.  There is no evidence of dysplasia or malignancy.   | Reactive lymphoid hyperplasia.  | `72000`
