---
title: "Miscellaneous"
date: 04/25/20 00:55:28
---

Code | Text | Summary | SNOMED
--- | --- | --- | ---
`@NVAS` | A and B) Sections show completely transected vas deferens.  There is no evidence of malignancy.   | A and B) Bilateral vasectomy -- Complete transection.  | `00100`
`@ACSY` | Synovial tissue with acute inflammation and degenerative changes consistent with acute synovitis.  There is no evidence of malignancy.  Correlate with microbiology.  No crystalline arthropathy/autoimmune disorder seen.  | Acute synovitis.  | `41000`
`@ALIP` | This consists of mature adipose tissue admixed with arteriole-sized blood vessels consistent with a benign angiolipoma. There are no atypical features.  | Angiolipoma.  | `88500`
`@BXO` | Sections show features of balanitis xerotica obliterans.  There is no evidence of dysplasia or malignancy.  | Balanitis xerotica obliterans.  | `58240`
`@PRSH` | These are fragments of prostatic parenchyma containing hyperplastic glands and stroma. There is no in situ or invasive malignancy.  | Benign prostatic hyperplasia.  | `72000`
`@GCTS` | The entire specimen has been processed.  Sections show a circumscribed, capsulated tumour composed of sheets of histiocyte-like cells with eosinophilic cytoplasm and vesicular nuclei.  Scattered osteoclast-like giant cells and some xanthoma cells are seen.  There is no evidence of atypia, mitoses, necrosis or malignancy.   | Features are of a giant cell tumour of the tendon sheath.  | `47830`
`@FEMR` | Sections show bone and haemopoietic marrow with features in keeping with fracture site.  There is no evidence of malignancy.  | Features in keeping with fracture site.  | `09350`
`@TAGCA` | This is a segment of artery consistent with the temporal artery. There is disruption of the internal elastic lamina and an inflammatory infiltrate is present including occasional multinucleate giant cells. The appearances are consistent with giant (temporal) cell arteritis.  | Giant cell arteritis.  | `44000`
`@GRNL` | Sections show a collection of epithelioid histiocytes; giant cells are also noted. There is no evidence of malignancy.  | Granulomatous inflammation.  Differential diagnosis includes tuberculosis and sarcoidosis as well as other chronic infections or autoimmune diseases.  Correlate clinically.  | `44000`
`@PRSN` | These cores consist of prostatic parenchyma showing no in situ or invasive malignancy.  | N/A | `N/A`
`@EMBO` | Sections show an organising fibrin thrombus in keeping with an embolus.  There is no evidence of malignancy.  | In keeping with an embolus.  | `35300`
`@LIPO` | This consists of mature adipose tissue consistent with a benign lipoma. There are no atypical features.  | Lipoma.  | `88500`
`@TARNS` | This is a segment of artery consistent with the temporal artery. The internal elastic lamina is disrupted in places but there is no inflammation in the sections examined.  | No inflammation seen.  | `09350`
`@PRSY` | Entire specimen processed.  Sections show multiple fragments composed of papillary synovial tissue with chronic inflammatory cell infiltrate.  There is no evidence of acute inflammation, neoplasia.  No rheumatoid or crystalline arthropathy seen.   | Proliferative synovitis.  | `09350`
`@TARN` | This is a segment of artery consistent with the temporal artery. There is no evidence of arteritis and the internal elastic lamina appears intact.  | Within normal limits.  | `00100`
`@INSP` | The specimen is inadequate for assessment of this lesion.  | N/A | `N/A`
`@NAIL` | The entire specimen has been processed.  Sections show fragments of keratinous material with one fragment showing overlying specialised skin, which probably represents the nail bed.  There is no evidence of inflammation or malignancy in material seen.   | No diagnostic features seen.  | `09350`
`@FSK` | Sections show skin with no evidence of dysplasia, malignancy or Balanitis Xerotica Obliterans.  | No diagnostic features seen.  | `09350`
`@SYNC` | Sections show synovial tissue with minimal chronic inflammation.  No evidence of active inflammation, crystalline arthropathy or an autoimmune disorder is noted.  | Synovial tissue with minimal chronic inflammation.  | `43000`
