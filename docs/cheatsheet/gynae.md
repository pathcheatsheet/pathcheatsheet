---
title: "Gynaecological and COLP"
date: 04/25/20 00:55:28
---

Code | Text | Summary | SNOMED
--- | --- | --- | ---
`@STER` | A and B)  Sections from each specimen show a completely transected Fallopian tube.  | A and B) Fallopian tubes -- Complete transection.  | `00100`
`@CIN1` | Multiple levels seen show squamous and glandular epithelium of the cervix with HPV effect and CIN 1.  There is no evidence of CGIN or malignancy.  | CIN 1. Cytology and histology results (do not) correlate.  | `74006`
`@CIN2` | Multiple levels seen show squamous and glandular epithelium of the cervix with HPV effect and CIN 2.  There is no evidence of CGIN or malignancy.  | CIN 2. Cytology and histology results (do not) correlate.  | `74007`
`@CIN3` | Multiple levels seen show squamous and glandular epithelium of the cervix with HPV effect and CIN 3.  There is no evidence of CGIN or malignancy.  | CIN 3. Cytology and histology results (do not) correlate.  | `74008?????`
`@CXIN` | Multiple levels seen show cervical tissue with no evidence of HPV or neoplasia.  | Cervical tissue with no evidence of HPV or neoplasia.  Note that there is no evidence of sampling of the squamocolumnar junction and that this may limit assessment of an intraepithelial lesion.  Correlation with colposcopic findings is recommended.  Discussion at MDT may be considered.  | `09011`
`@HPV` | Multiple levels seen show squamous and glandular epithelium of the cervix with HPV effect.  There is no evidence of  CIN, CGIN or  malignancy.  | Cervix -- HPV effect only. Cytology and histology results (do not) correlate.  | `69470`
`@CXN` | Multiple levels seen show squamous and glandular epithelium of the cervix with no evidence of HPV or neoplasia.  | Cervix -- No evidence of HPV or neoplasia. Cytology and histology results (do not) correlate.  | `09350`
`@CHEND` | Sections show proliferative/secretory/inactive??? endometrium. Stroma is composed of spindle cells with scattered lymphocytes and plasma cells therein. No evidence of hyperplasia, polyp formation or malignancy is noted.  | Chronic nonspecific endometritis.  | `43000`
`@DECO` | The entire specimen has been processed.  Sections show decidual tissue only.  No chorionic villi or fetal parts seen.   | Decidual tissue only; consistent with recent intra-/extrauterine gestation.  | `09350 or 28000`
`@CPOL` | Sections show a benign endocervical polyp.  There is no evidence of dysplasia or malignancy.  | Endocervical polyp.  | `76800`
`@EPOL` | Benign endometrial polyp with no evidence of neoplasia.  | Endometrial polyp.  | `76800`
`@GEEN` | Sections show endometrium with atrophic glands and pseudodecidualised stroma. There is no evidence of neoplasia.  | Endometrium showing gestagen effect.  | `decidual`
`@PGEEN` | Sections show endometrium with atrophic glands and pseudodecidualised stroma. There is no evidence of neoplasia.  | Endometrium suggesting exogenous hormone effect; any hystory?  | `decidual`
`@INEN` | Curettings show inactive/weakly proliferative endometrium.  There is no evidence of endometritis or neoplasia.  | Inactive/weakly proliferative endometrium.  | `79360`
`@SCEN` | Sections show blood and scanty strips of endometrial tissue. No neoplasia is seen but specimen is unassessable.  | Unassessable; please see above.  | `09011`
`@IVEN` | Interval phase endometrium with no endometritis, hyperplasia or malignancy.  | Interval phase endometrium .  | `09350`
`@KRAU` | Sections show features of lichen sclerosus et atrophicus.  There is no evidence of dysplasia or malignancy.  | Lichen sclerosus et atrophicus.  | `58240`
`@MEEN` | Menstrual phase endometrium.  There is no evidence of endometritis or neoplasia.  | Menstrual phase endometrium.  | `????????`
`@NOEN` | Sections show no evidence of endometrial sampling and hence this is inadequate for assessment.  | No evidence of endometrial sampling -- inadequate.  | `09011`
`@PLN` | Third trimester placenta with unremarkable membranes and chord; no significant pathology is noted.  | No significant pathology.  | `09350`
`@POC` | Sections show chorionic villi and decidual tissue.  There is no evidence of gestational trophoblastic disease.  No fetal parts are seen.   | Products of conception confirmed.  | `09350 or 28000`
`@PDEC` | Pseudodecidualisation is also seen -- may be attributable to exogenous hormones (any history?).  | N/A | `N/A`
`@PREN` | Sections show features of proliferative endometrium.  There is no evidence of endometritis or neoplasia.  | Proliferative endometrium.  | `79310`
`@SECEN` | Sections show features of secretory endometrium.  There is no evidence of endometritis or neoplasia.  | Secretory endometrium .  | `79320`
`@BEUT` | Cervix shows no evidence of neoplasia. Both Fallopian tubes and ovaries are unremarkable. Endometrium is inactive/proliferative/secretory; there is no evidence of inflammation, polyp or nmeoplasia. The grossly described fibroids are benign leiomyomata showing no atypical features.  | Uterus -- Leiomyoma.  | `anything`
