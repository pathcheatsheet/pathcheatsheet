-- Uncomment the line below to add SNOMED to the reports' ends 
-- (might come in handy when using Winpath):

AddSnomed = true

local AHK = {}
local MD = 
{
  ["%*.-"] = "^i", -- *italic*
  ["%*%*.-"] = "^b", -- ****bold****
  ["_.-"] = "^b", -- __bold__
  ["__.-"] = "^i", -- italic
  ["~~"] = "^u", -- underscore (strikethrough has no Word hotkey)
  ["#.-"] = "+{Home}^|1{End}{Enter}", -- H1
  ["##.-"] = "+{Home}^|2{End}{Enter}", -- H2
  ["###.-"] = "+{Home}^|3{End}{Enter}" -- H3
  }
if os.getenv("windir") then -- Windows
  f = io.popen("dir docs\\cheatsheet /b")
else -- non-Windows: POSIX-compliant
  f = io.popen("ls -1 docs/cheatsheet") 
end
for line in f:lines() do
  local filename, ext = line:match("([^%.]+%.md)$")
  if filename then
    filename = "docs/cheatsheet/"..filename
    AHK[filename] = AHK[filename] or {}
    for line in io.lines (filename) do
      local code, text, summary, SNOMED = line:match("`(.+)`%s*|%s*(.*)%s*|%s*"
      .."(.*)%s*|%s*`(.+)`")
      if SNOMED then
        for PM, HK in pairs (MD) do
          text = text:gsub(PM, HK)
          summary = summary:gsub(PM, HK)
        end
        if SNOMED == "x" then
          table.insert(AHK[filename], "::"..code.."::"..text)
        else
          if AddSnomed then
            table.insert(AHK[filename], "::"..code.."::"
--            .."MICRO:{Enter}"..text.."{Enter}{Down}{Down}"
--            .."MICRO:{Enter}"..text.."{Enter}^{End}"
            ..text.."{Enter}{Enter}"
            .."^bSUMMARY:^b{Enter}"..summary.."{Enter}{Enter}"..SNOMED)
          else
            table.insert(AHK[filename], "::"..code.."::"
            .."MICRO:{Enter}"..text.."{Enter}^{End}"
            ..summary)
          end
        end
      end
    end
  end
end
f:close(); f = io.open ("cheatsheet.ahk","w+")
for section, array in pairs (AHK) do
  f:write(";\r\n;\r\n;"..section.."\r\n;\r\n")
  for _,v in ipairs(array) do
    f:write(v.."\r\n")
  end
end
f:write("::@sig::\r\n"
.."FormatTime, CurrentDateTime,, dd/MM/yyyy\r\n"
.."SendInput {Enter}{Enter}^bDr Gergo Szakal^b{Enter}Locum Consultant Histopathologist"
.."{Enter}%CurrentDateTime%\r\nreturn")
f:close()
